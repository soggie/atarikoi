# Atarikoi Codebase

## Architecture overview

Atarikoi's website involves 3 services: Cloudflare, Contentful and Gitlab.

**Cloudflare** - the domain atarikoi.com is managed through Cloudflare, which also manages its SSL certification.

**Contentful** - this service is used to manage content for Atarikoi. All categories (called catalogs) and products are generated on contentful.

**Gitlab** - this service stores the code for Atarikoi, and doubles as a host for the webpages (through gitlab pages).

## How it works together

**Serving content to users** - when a user types in "www.atarikoi.com" into the web browser, their browser will reach Cloudflare first, which will then be redirected to Gitlab pages.

**Product management** - admin logs into Contentful, adds/edits/deletes catalogs or products. Upon saving, it will trigger Contentful to notify Gitlab about the changes, and Gitlab will start a _build_ which will then generate all the static HTML pages.

## Content generation pipeline

Technologies used:

* Jekyll - static site generator
* Jekyll contentful gem - custom plugin to convert contentful data to static pages

## Setting up local development environment

1. Install jekyll (https://jekyllrb.com/docs/installation/)
2. Git clone the repository
3. Inside the repository directory, type `jekyll contentful` to download the content from Contentful (see `src/_config.yml` under `contentful` option for config details)
4. Type `jekyll build` or `jekyll serve` to build the site/build and serve it locally respectively
5. View it in browser

## Setting up Gitlab CI build pipeline

1. Go to gitlab
2. Enable the gitlab runner
3. Look at `src/.gitlab-ci.yml` for instructions
4. `git push` to gitlab
5. Check in gitlab page under runners to see if the build is successful

## Connecting contentful to gitlab

There's a webhook in contentful that is set up so that any content updates in contenful will trigger a build in gitlab, thus updating the website.

1. Log in to contentful
2. Go to `settings` and then `webhooks`
3. Either add a new webhook or edit the existing one

## Technical decisions

The solution needs to fulfil these requirements:

1. Client needs to update content on their own
2. Client should not be burdened to pay for what they don't need
3. Project should be transferable to other parties without change in infra
4. No propriety or non-open sourced libraries should be used

The solution thought process:

1. Based on the above requirements, we will need to leverage free services
2. Gitlab offers static site hosting and a build pipeline. Perfect for #2 and #4
3. Jekyll chosen as a static site generator due to ruby's accessibility (highly recommended to switch to hugo for speed in the future)
4. Contentful chosen as a content manager due to ease of use
5. Contentful has webhooks that can link to gitlab builder
6. Cloudflare chosen as a proxy due to its trustworthiness