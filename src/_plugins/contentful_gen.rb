module Jekyll
  class CategoryPageGenerator < Generator
    safe true

    def generate(site)
      if site.layouts.key? 'category_index'
        dir = site.config['category_dir'] || 'categories'

        site.data["contentful"]["spaces"]["links"]["catalog"].each do |category|
          site.pages << CategoryPage.new(site, site.source, File.join(dir), category)
        end
      end
    end
  end

  class CategoryPage < Page
    def initialize(site, base, dir, category)
      @site = site
      @base = base
      @dir = dir
      @name = category["slug"] + '.html'

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'category_index.html')
      self.data['category'] = category

      category_title = category["catalog_name"]
      self.data['title'] = "#{category_title}"
      self.data['subtitle'] = "Quality Guaranteed"
      self.data['menu'] = "store"
    end
  end

  class ProductPageGenerator < Generator
    safe true

    def generate(site)
      if site.layouts.key? 'product_index'
        dir = site.config['product_dir'] || 'products'

        products = site.data["contentful"]["spaces"]["links"]["product"]
        catalogs = site.data["contentful"]["spaces"]["links"]["catalog"]

        # Turn products into a hash based off its key
        products_hash = Hash.new
        products.each { |p| products_hash[p["sys"]["id"]] = p }

        # Attach the category to each product
        catalogs.each do |catalog|
          catalog['products'].each do |p|
            products_hash[p['sys']['id']]['category'] = catalog
          end
        end
      
        products.each do |product|
          site.pages << ProductPage.new(site, site.source, File.join(dir), product)
        end
      end
    end
  end

  class ProductPage < Page
    def initialize(site, base, dir, product)
      @site = site
      @base = base
      @dir = dir
      @name = product["slug"] + '.html'

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'product_index.html')
      self.data['product'] = product

      product_title = product["title"]
      self.data['title'] = "Product"
      self.data['subtitle'] = "Quality Guaranteed"
      self.data['menu'] = "store"
    end
  end
end